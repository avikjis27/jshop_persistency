/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.List;
import org.hibernate.Session;
import persistency.HibernateActivity;
import persistency.ItemType;

/**
 *
 * @author IBM_ADMIN
 */
public class DataBaseState {
    public static List<ItemType> getAllItemType() {
        Session session=HibernateActivity.getHibernateSession();
        
        if(session!=null)
        {   
            session.beginTransaction();
            List<ItemType> itemType=(List<ItemType>) session.createQuery("from ItemType").list();
            
            session.getTransaction().commit();
            
            
            
            
            return itemType;
        }
        else
            return null;
    }
    
}
