/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Properties;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author avik
 */
public class HibernateActivity {
    
    private static SessionFactory sessionFactory;
    private static Session session;
    private static Configuration configuration;
    
    
    public static Configuration getHibernateConfiguration()
    {
        if(configuration==null) {
            return null;
        }
        else {
            return configuration;
        }
    }
    
    
    
    
    public static Session getHibernateSession()
    {   System.out.println("getHibernateSession called");
        
        if(sessionFactory==null)
        {   System.out.println("New hibernate session factory is goint to crate");
            try{    
                    configuration=new Configuration();
                    sessionFactory=configuration.configure().buildSessionFactory();
                    System.out.println("2");
                    session=sessionFactory.openSession();
                    System.out.println("3");
                    System.out.println("hibernate session and factory successfully opened");
                }
            catch (HibernateException he)
            {
                System.out.println("Hibernate exception caught:"+he);
            
            }
            finally
            {
                return session;
            }
            
        }
        else if(session==null)
        {   System.out.println("New hibernate session is goint to crate with existing session factory");
            try{
                    
                    session=sessionFactory.openSession();
                    System.out.println("hibernate session successfully opened");
                }
            catch (HibernateException he)
            {
                System.out.println("Hibernate exception caught:"+he);
            
            }
            finally
            {
                return session;
            }
        }
        else
        {   System.out.println("reusing existing hibernate session");
            return session;
        }
        //System.out.println("nothing");
    }
    
    public static boolean closeHibernateSession()
    {   if(!session.isOpen())
            return false;
        else{
           
            session.close();
            sessionFactory.close();
            session=null;
            sessionFactory=null;
            return true;
        }
    }
    
    
    
    
}
