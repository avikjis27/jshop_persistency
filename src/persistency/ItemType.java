/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author avik
 */
//@Entity
public class ItemType {
    
    //@Id
    //@GenericGenerator(name="Generator", strategy="increment")
    //@GeneratedValue(generator="Generator")
    //@Column(name="Id")
    private int id;
    private String itemName;

    public ItemType() {
    }

    public ItemType(String itemName) {
        this.itemName = itemName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    
    
    
    
}
