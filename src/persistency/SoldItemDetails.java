/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

/**
 *
 * @author avik
 * 
 */
public class SoldItemDetails {
    private long id;
    private long custId;
    private String itemModelNumber;
    private String itemId;
    private String itemType;
    private float MRP;
    private float vat;
    private String manufacturer;
    private float sellPrice;
    private float discount;

    public SoldItemDetails(long custId, String itemModelNumber, String itemId, String itemType, float MRP, float vat, String manufacturer, float sellPrice,float discount) {
        
        this.custId = custId;
        this.itemModelNumber = itemModelNumber;
        this.itemId = itemId;
        this.itemType = itemType;
        this.MRP = MRP;
        this.vat = vat;
        this.manufacturer = manufacturer;
        this.sellPrice = sellPrice;
        this.discount=discount;
    }
    
    

    
    

    public SoldItemDetails() {
    }
    
    

    public float getMRP() {
        return MRP;
    }

    public void setMRP(float MRP) {
        this.MRP = MRP;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }
    

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemModelNumber() {
        return itemModelNumber;
    }

    public void setItemModelNumber(String itemModelNumber) {
        this.itemModelNumber = itemModelNumber;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    
    public float getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }

    public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }
   
    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }
    
    
    
}
