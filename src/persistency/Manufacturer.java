/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author avik
 */
//@Entity
public class Manufacturer {
    
    private int Id;
    private String manufacturerName;

    public Manufacturer() {
    }

    public Manufacturer(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
   // @Id
    //@GenericGenerator(name="Generator", strategy="increment")
    //@GeneratedValue(generator="Generator")
    //@Column(name="Id")
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
    
    
    
    
    
    
    
}
