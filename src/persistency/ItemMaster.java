/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;


/**
 *
 * @author avik
 */


public class ItemMaster {
    
    private long id;
    private String itemModelNumber;
    private String itemId;
    private String itemType;
    private float MRP;
    private float purchasePrice;
    private float vat;
    private String manufacturer;
    private static int instanceCount;

    public ItemMaster() {
        if(instanceCount==1)
        {   
            System.out.println("Instance count more than one:");
            instanceCount++;
            
        }
    }

    
    public ItemMaster(String itemModelNumber, String itemId, String itemType, float MRP, float purchasePrice, float vat, String manufacturer) {
        
        
        if(instanceCount==1)
        {   
            System.out.println("Fatal:instance count more than one:");
            instanceCount++;
        }
        else
            instanceCount=1;
        this.itemModelNumber = itemModelNumber;
        this.MRP = MRP;
        this.purchasePrice = purchasePrice;
        this.vat = vat;
        this.manufacturer = manufacturer;
        this.itemType=itemType;
        
        this.itemId=itemId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public float getMRP() {
        return MRP;
    }

    public void setMRP(float MRP) {
        this.MRP = MRP;
    }

   

    public String getItemModelNumber() {
        return itemModelNumber;
    }

    public void setItemModelNumber(String itemModelNumber) {
        this.itemModelNumber = itemModelNumber;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    

    public float getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(float purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }
}
