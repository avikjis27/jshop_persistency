/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

/**
 *
 * @author avik
 */
//@Entity
public class UserDetails {
  
    private String userName;
    private String password;
    private String userRole;
    private String fName;
    private String lName;

    public UserDetails() {
    }
    
    
    public UserDetails(String userName,String password,String role,String fName,String lName)
    {
        
        this.userName=userName;
        this.password=password;
        this.userRole=role;
        this.fName=fName;
        this.lName=lName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

   
}
