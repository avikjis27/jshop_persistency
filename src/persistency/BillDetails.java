/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author avik
 */


public class BillDetails {

    private long id;
    private String invoiceNumber;
    private long customerId=-1;
    private Date billDate;
    private Set<SoldItemDetails> items=new HashSet();
    private float billAmount;
    private static BillDetails bd;
    
    
    public BillDetails(){
        this.billDate = new Date();
    }

   
    /*public BillDetails(String invoiceNumber) {
        
        this.invoiceNumber = invoiceNumber;
        this.billDate = new Date();
    }*/
    public String[][] getItemsAsArray()
    {
        String[][] itemAsArray= new String[items.size()][2];
        Iterator ite=items.iterator();
        int i=0;
        while(ite.hasNext())
        {
            
           SoldItemDetails temp=(SoldItemDetails)ite.next();
           itemAsArray[i][0]=temp.getItemId();
           itemAsArray[i][1]=temp.getItemModelNumber();
           i++;
        }
        return itemAsArray;
    }
    public static BillDetails getBillDetails(){
        return bd;
    }
    public static BillDetails billDetailsFactory(){
        if(bd==null)
        {   System.out.println("Creating new bill details");
            bd=new BillDetails();
            bd.setItems();
            return bd;
        }
        else
        {   System.out.println("Using Existing bill details:"+bd);
            return bd;
        }
    
    }
    public static boolean closeBillDetailsFactory(){
        if(bd==null)
        {
            System.out.println("Bill details already closed:"+bd);
            return true;
        }
        else
        {
            System.out.println("Closing Bill details "+bd);
            bd=null;
            return true;
        }
    
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }
    

    public float getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(float billAmount) {
        this.billAmount = billAmount;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    
    public Set<SoldItemDetails> getItems() {
        return items;
    }

    public void setItems(Set<SoldItemDetails> items) {
        this.items = items;
    }
    

    private void setItems() {
        this.items = new HashSet<SoldItemDetails>();
    }
   
   
    
}
