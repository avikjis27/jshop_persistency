/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;




/**
 *
 * @author avik
 */

public class StockReplenishmentDetails {
   
   
    private long stockReplenishmentNumber;
    private Set<ItemMaster> items=new HashSet();
    private Date stockEntryDate=new Date();
    private static StockReplenishmentDetails srd;
    
    //@JoinTable(name="Stock_Entry_dtls",joinColumns=@JoinColumn(name="hELLO"))
    public StockReplenishmentDetails() {
        srd=this;
        
    }
    public static StockReplenishmentDetails stockReplenishmentDetailsFactory(){
        if(srd==null)
        {
            srd=new StockReplenishmentDetails();
            return srd;
        }
        else
        {
            return srd;
        }
    
    }
    public static boolean closeStockReplenishmentDetailsFactory(){
        if(srd==null)
        {
            
            return true;
        }
        else
        {
            srd=null;
            return true;
        }
    
    }
    public Set<ItemMaster> getItems() {
        return items;
    }

    private void setItems(Set<ItemMaster> items) {
        this.items = items;
    }
    
    public void deleteItem(String itemId,String modelNumber)
    {   System.out.println("In function deleteItem with parameter:ItemId:"+itemId+" and model number:"+modelNumber);
        
        Iterator ite=items.iterator();
        while(ite.hasNext())
        {
           ItemMaster temp=(ItemMaster)ite.next();
           if(temp.getItemId().equals(itemId)&&temp.getItemModelNumber().equals(modelNumber))
           {
               System.out.println("Going to remove item:\n\tItemID:"+temp.getItemId()+"\n\tModel Number:"+temp.getItemModelNumber());
               ite.remove();
           }
        }
    }
    
    

    

    public Date getStockEntryDate() {
        return stockEntryDate;
    }

    public void setStockEntryDate(Date stockEntryDate) {
        this.stockEntryDate = stockEntryDate;
    }
    
    public long getStockReplenishmentNumber() {
        return stockReplenishmentNumber;
    }

    public void setStockReplenishmentNumber(long stockReplenishmentNumber) {
        this.stockReplenishmentNumber = stockReplenishmentNumber;
    }
    
    
    
    
}
