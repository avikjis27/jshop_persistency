/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistency;

/**
 *
 * @author IBM_ADMIN
 */
public class CustomerMaster {
    private long id;
    private String customerName;
    private String customerAddress;
    private String PIN;
    private String state;
    private String city;
    private String contact;

    public CustomerMaster() {
    }

    public CustomerMaster(String customerName, String customerAddress, String PIN, String state, String city, String contact) {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.PIN = PIN;
        this.state = state;
        this.city = city;
        this.contact = contact;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPIN() {
        return PIN;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
    
    
}
